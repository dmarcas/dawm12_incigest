<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"  %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <link href="<c:url value="/estilos/css/home.css" />" rel="stylesheet">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">
        <script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.3.1.min.js" /></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.bundle.min.js" /></script>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
<title>INCIGEST</title>
</head>
<body id="page-top">
    <nav class="navbar navbar-expand navbar-dark bg-dark static-top">
        <a class="navbar-brand mr-1" href="<spring:url value="/home"/>">INCIGEST</a>
        <!-- Navegador superior -->
        <ul class="navbar-nav ml-auto mr-0 mr-md-3 my-2 my-md-0">
            <a class="navbar-brand mr-1" href="<spring:url value="/home"/>">${userLogged}</a>
            <li class="nav-item dropdown no-arrow">
                <a class="nav-link dropdown-toggle" href="<spring:url value="/j_spring_security_logout"/>" id="userDropdown" role="button">
                    <i class="fas fa-sign-out-alt"></i>
                </a>
            </li>
        </ul>
    </nav>
    <div id="wrapper">
        <!-- Barra lateral -->
        <ul class="sidebar navbar-nav">            
            <c:if test="${userAuthority eq 'Admin'}">
                <li class="nav-item">
                    <a class="nav-link" href="<spring:url value="/home/adminHome/users"/>">
                        <i class="fas fa-user"></i>
                        <span>Usuaris</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<spring:url value="/home/adminHome/incidentTypes"/>">
                        <i class="fas fa-sitemap"></i>
                        <span>Tipus Incidències</span></a>
                </li>
            </c:if>
            <li class="nav-item active">
                <a class="nav-link" href="<spring:url value="/home/incidents"/>">
                    <i class="fas fa-wrench"></i>
                    <span>Incidències</span>
                </a>
            </li>
        </ul>
        <div id="content-wrapper">
            <div class="container-fluid">
                <a class="breadcrumb">Crear incidencies: </a>
                <form:form class="form-signin" modelAttribute="formAddIncident" action="addIncident">               
                    <form:input type="hidden" id="createdBy" path="createdBy.userName" value="${currentUser}"/>
                    <label for="inputTitle">Títol</label>
                    <div class="form-label-group">
                        <form:input id="title" path="title" type="text" class="form-control"/>
                    </div>
                    <label for="inputDescription">Descripció</label>
                    <div class="form-label-group">
                        <form:textarea id="description" path="description" type="text" class="form-control"/>
                    </div>
                    <label for="incidentType">Tipus incident</label>
                    <div class="form-label-group">
                        <form:select id="type" path="type.nameType" class="form-control">
                            <form:options items="${incidentTypes}" itemValue="nameType" itemLabel="nameType"/>                            
                        </form:select>
                    </div> 
                    <c:if test="${userAuthority eq 'Admin'}">
                        <label for="inputTechnicAssigned">Tècnic assignat</label>
                        <div class="form-label-group">
                            <form:select id="technicAssigned" path="technicAssigned.userName" class="form-control">
                                <form:options items="${technics}" itemValue="userName" itemLabel="userName"/>
                            </form:select>
                        </div>        
                    </c:if>
                    <c:if test="${userAuthority eq 'Admin' or userAuthority eq 'Tecnic'}">
                        <label for="inputStatus">Estat</label>
                        <div class="form-label-group">
                            <form:select id="status" path="status" items="${status}" type="text" class="form-control"/>
                        </div>
                    </c:if>
                    </br>          
                    <input type="submit" id="btnAdd" class="btn btn-primary" value="Guardar"/>
                    <a href="<spring:url value="/home/incidents"/>" class="btn btn-secondary">Cancel·lar</a>
                </form:form>
                <c:if test="${not empty error}">
                    <div class="${alertType}">${incorrectregister}</div>
                </c:if>
                </br>
            </div>
        </div>                   
    </div> 
</body>
</html>
