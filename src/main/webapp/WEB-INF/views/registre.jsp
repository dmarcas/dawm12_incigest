<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"  %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <link href="<c:url value="/estilos/css/registre.css" />" rel="stylesheet">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">
        <script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.3.1.min.js" /></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.bundle.min.js" /></script>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
<title>INCIGEST</title>
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
                <div class="card card-signin my-5">
                    <div class="card-body">
                        <h5 class="card-title text-center">${benvinguda}</h5>
                        <form:form class="form-signin" modelAttribute="formRegistre" action="registre/add">
                            <label for="inputUserName">Nom d'usuari</label>
                            <div class="form-label-group">
                                <form:input id="userName" path="userName" type="text" class="form-control"/>
                            </div>
                            <label for="inputPassword">Password</label>
                            <div class="form-label-group">
                                <form:input id="password" path="password" type="text" class="form-control"/>
                            </div>
                            <label for="inputDepartment">Departament</label>
                            <div class="form-label-group">
                                <form:input id="department" path="department" type="text" class="form-control"/>
                            </div>
                            <input type="submit" id="btnAdd" class="btn btn-lg btn-primary btn-block text-uppercase" value="Registra't"/>
                        </form:form>
                        <br/>
                        <a href="<spring:url value="/"/>" class="btn btn-secondary">Tornar a Login</a>
                        <br/>
                        <br/>
                        <c:if test="${not empty error}">
                            <div class="${alertType}">${incorrectregister}</div>
                        </c:if>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>