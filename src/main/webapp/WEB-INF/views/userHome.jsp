<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"  %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <link href="<c:url value="/estilos/css/home.css" />" rel="stylesheet">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">
        <script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.3.1.min.js" /></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.bundle.min.js" /></script>
<script type="text/javascript">
    $(document).ready(function () {
        var casilla = $('.casilla');
        casilla.find('.card-footer').hide();
        $(casilla).hover(function () {
            $(this).find('.card-footer').toggle();
        });
    });
</script>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
<title>INCIGEST</title>
</head>
<body id="page-top">
    <nav class="navbar navbar-expand navbar-dark bg-dark static-top">
        <a class="navbar-brand mr-1" href="<spring:url value="/home"/>">INCIGEST</a>
        <!-- Navegador superior -->
        <ul class="navbar-nav ml-auto mr-0 mr-md-3 my-2 my-md-0">
            <a class="navbar-brand mr-1" href="<spring:url value="/home"/>">${userLogged}</a>
            <li class="nav-item dropdown no-arrow">
                <a class="nav-link dropdown-toggle" href="<spring:url value="/j_spring_security_logout"/>" id="userDropdown" role="button">
                    <i class="fas fa-sign-out-alt"></i>
                </a>
            </li>
        </ul>
    </nav>
    <div id="wrapper">
        <!-- Barra lateral -->
        <ul class="sidebar navbar-nav">
            <li class="nav-item">
                <a class="nav-link" href="<spring:url value="/home/incidents"/>">
                    <i class="fas fa-wrench"></i>
                    <span>Incidències</span>
                </a>
        </ul>
        <div id="content-wrapper">
            <div class="container-fluid">
                <!-- Zona interior -->
                <div class="row">
                    <div class="col-xl-4 col-sm-6 mb-3">
                        <div class="casilla card text-white o-hidden h-100" style="background-color:DodgerBlue;">
                            <div class="card-body">
                                <div class="mr-5">
                                    <a >${incidNoves} Noves</a>
                                </div>
                            </div>
                            <a class="card-footer text-white clearfix small z-1" href="<spring:url value="/home/userHome/incidents/totalNoves"/>">
                                <span class="float-left">Veure incidències</span>
                                <span class="float-right">
                                    <i class="fas fa-angle-right"></i>
                                </span>
                            </a>
                        </div>
                    </div>
                    <div class="col-xl-4 col-sm-6 mb-3">
                        <div class="casilla card text-white o-hidden h-100" style="background-color:green;">
                            <div class="card-body">
                                <div class="mr-5">
                                    <a >${incidEnCurs} En curs</a>
                                </div>
                            </div>
                            <a class="card-footer text-white clearfix small z-1" href="<spring:url value="/home/userHome/incidents/totalEnCurs"/>">
                                <span class="float-left">Veure incidències</span>
                                <span class="float-right">
                                    <i class="fas fa-angle-right"></i>
                                </span>
                            </a>
                        </div>
                    </div>
                    <div class="col-xl-4 col-sm-6 mb-3">
                        <div class="casilla card text-white o-hidden h-100" style="background-color:yellowgreen;">
                            <div class="card-body">
                                <div class="mr-5">
                                    <a >${incidResoltes} Resoltes</a></div>
                            </div>
                            <a class="card-footer text-white clearfix small z-1" href="<spring:url value="/home/userHome/incidents/totalResoltes"/>">
                                <span class="float-left">Veure incidències</span>
                                <span class="float-right">
                                    <i class="fas fa-angle-right"></i>
                                </span>
                            </a>
                        </div>
                    </div>
                    <div class="col-xl-4 col-sm-6 mb-3">
                        <div class="casilla card text-white o-hidden h-100" style="background-color:gray;">
                            <div class="card-body">
                                <div class="mr-5">
                                    <a >${incidTancades} Tancades</a></div>
                            </div>
                            <a class="card-footer text-white clearfix small z-1" href="<spring:url value="/home/userHome/incidents/totalTancades"/>">
                                <span class="float-left">Veure incidències</span>
                                <span class="float-right">
                                    <i class="fas fa-angle-right"></i>
                                </span>
                            </a>
                        </div>
                    </div>
                    <div class="col-xl-4 col-sm-6 mb-3">
                        <div class="casilla card text-white o-hidden h-100" style="background-color:turquoise;">
                            <div class="card-body">
                                <div class="mr-5">
                                    <a >${incidTotals} Totals</a></div>
                            </div>
                            <a class="card-footer text-white clearfix small z-1" href="<spring:url value="/home/userHome/incidents/incidenciesTotals"/>">
                                <span class="float-left">Veure incidències</span>
                                <span class="float-right">
                                    <i class="fas fa-angle-right"></i>
                                </span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
