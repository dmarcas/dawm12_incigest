<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"  %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <link href="<c:url value="/estilos/css/home.css" />" rel="stylesheet">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">
        <script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.3.1.min.js" /></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.bundle.min.js" /></script>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
<title>INCIGEST</title>
</head>
<body id="page-top">
    <nav class="navbar navbar-expand navbar-dark bg-dark static-top">
        <a class="navbar-brand mr-1" href="<spring:url value="/home"/>">INCIGEST</a>
        <!-- Navegador superior -->
        <ul class="navbar-nav ml-auto mr-0 mr-md-3 my-2 my-md-0">
            <a class="navbar-brand mr-1" href="<spring:url value="/home"/>">${userLogged}</a>
            <li class="nav-item dropdown no-arrow">
                <a class="nav-link dropdown-toggle" href="<spring:url value="/j_spring_security_logout"/>" id="userDropdown" role="button">
                    <i class="fas fa-sign-out-alt"></i>
                </a>
            </li>
        </ul>
    </nav>
    <div id="wrapper">
        <!-- Barra lateral -->
        <ul class="sidebar navbar-nav">
            <c:if test="${userAuthority eq 'Admin'}">
                <li class="nav-item">
                    <a class="nav-link" href="<spring:url value="/home/adminHome/users"/>">
                        <i class="fas fa-user"></i>
                        <span>Usuaris</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<spring:url value="/home/adminHome/incidentTypes"/>">
                        <i class="fas fa-sitemap"></i>
                        <span>Tipus Incidències</span></a>
                </li>
            </c:if>
            <li class="nav-item active">
                <a class="nav-link" href="<spring:url value="/home/incidents"/>">
                    <i class="fas fa-wrench"></i>
                    <span>Incidències</span>
                </a>
            </li>
        </ul>
        <div id="content-wrapper">
            <div class="container-fluid">
                <br/>
                <a href="<spring:url value="/home/incidents/add"/>" class="btn btn-primary">Crear Incidencia</a>
                <a href="<spring:url value="/home/incidents/search"/>" class="btn btn-secondary">Buscar Incidencia</a>
                <br/>
                <br/>
                <a class="breadcrumb">Incidencies Noves:</a>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Títol</th>
                                    <th>Data Creació</th>
                                    <th>Usuari</th>
                                    <th>Tipus</th>
                                    <th>Estat</th>
                                    <th>Tecnic Assignat</th>
                                    <th>Opcions</th>
                                </tr>
                            </thead>
                            <tbody>
                                <c:forEach items="${incidenciesNoves}" var="incident">
                                    <tr>
                                        <td>${incident.incidentId}</td>
                                        <td>${incident.title}</td>
                                        <td>${incident.creationDate}</td>
                                        <td>${incident.createdBy.userName}</td>
                                        <td>${incident.type.nameType}</td>
                                        <td>${incident.status}</td>
                                        <td>${incident.technicAssigned.userName}</td>
                                        <td>
                                            <a class="nav-item" href="<spring:url value='/home/incidents/viewIncident?incidentId=${incident.incidentId}'/>" role="button">
                                                <i class="fas fa-info-circle"></i>
                                            </a>
                                            <c:if test="${userAuthority eq 'Admin' or userAuthority eq 'Tecnic'}">
                                                <a class="nav-item" href="<spring:url value='/home/incidents/modifyIncident?incidentId=${incident.incidentId}'/>" role="button">
                                                    <i class="fas fa-user-edit"></i>
                                                </a>
                                            </c:if> 
                                            <c:if test="${userAuthority eq 'Usuari' or userAuthority eq 'Admin'}">
                                                <a class="nav-item" href="<spring:url value='/home/incidents/delete?incidentId=${incident.incidentId}'/>" role="button">
                                                    <i class="fas fa-trash"></i>
                                                </a>
                                            </c:if>  
                                        </td>
                                    </tr>
                                </c:forEach>
                            </tbody>
                        </table>
                    </div>
                </div>
                <a class="breadcrumb">Incidencies en Curs:</a>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Títol</th>
                                    <th>Data Creació</th>
                                    <th>Usuari</th>
                                    <th>Tipus</th>
                                    <th>Estat</th>
                                    <th>Tecnic Assignat</th>
                                    <th>Opcions</th>
                                </tr>
                            </thead>
                            <tbody>
                                <c:forEach items="${incidencies}" var="incident">
                                    <tr>
                                        <td>${incident.incidentId}</td>
                                        <td>${incident.title}</td>
                                        <td>${incident.creationDate}</td>
                                        <td>${incident.createdBy.userName}</td>
                                        <td>${incident.type.nameType}</td>
                                        <td>${incident.status}</td>
                                        <td>${incident.technicAssigned.userName}</td>
                                        <td>
                                            <a class="nav-item" href="<spring:url value='/home/incidents/viewIncident?incidentId=${incident.incidentId}'/>" role="button">
                                                <i class="fas fa-info-circle"></i>
                                            </a>
                                            <c:if test="${userAuthority eq 'Admin' or userAuthority eq 'Tecnic'}">
                                                <a class="nav-item" href="<spring:url value='/home/incidents/modifyIncident?incidentId=${incident.incidentId}'/>" role="button">
                                                    <i class="fas fa-user-edit"></i>
                                                </a>
                                            </c:if>  
                                            <c:if test="${userAuthority eq 'Admin'}">
                                                <a class="nav-item" href="<spring:url value='/home/incidents/delete?incidentId=${incident.incidentId}'/>" role="button">
                                                    <i class="fas fa-trash"></i>
                                                </a>
                                            </c:if>  
                                        </td>
                                    </tr>
                                </c:forEach>                                
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>  
    </div> 
</body>
</html>