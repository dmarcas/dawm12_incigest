<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"  %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <link href="<c:url value="/estilos/css/login.css" />" rel="stylesheet">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">
        <script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.3.1.min.js" /></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.bundle.min.js" /></script>
<title>INCIGEST</title>
</head>
<body>
    <div class="container-fluid">
        <div class="row no-gutter">
            <div class="d-none d-md-flex col-md-4 col-lg-6 bg-image"></div>
            <div class="col-md-8 col-lg-6">
                <div class="login d-flex align-items-center py-5">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-9 col-lg-8 mx-auto">
                                <img class="img-fluid mb-5 d-block mx-auto" src="<c:url value="/estilos/image/incigestLogo.png"/>">
                                <!--h1> ${benvinguda} </h1-->
                                <!--p> ${tagline} </p-->
                                <div class="panel-body">
                                    <c:if test="${not empty error}">
                                        <div class="alert alert-danger">
                                            Credencials incorrectes
                                        </div>
                                    </c:if>
                                    <form action="<c:url value= "/j_spring_security_check"> </c:url>" method="post">
                                            <label for="inputEmail">Usuari</label>
                                            <div class="form-label-group">
                                                <input class="form-control" placeholder="Usuari" name='j_username' type="text">
                                            </div>
                                            <label for="inputPassword">Contrasenya</label>
                                            <div class="form-label-group">
                                                <input class="form-control" placeholder="Contrasenya" name='j_password' type="password">
                                            </div>
                                            <button class="btn btn-lg btn-primary btn-block btn-login text-uppercase font-weight-bold mb-2" type="submit">Entrar</button>
                                        </form>
                                        <a href="<spring:url value="/registre"/>" class="btn btn-secondary">Registra't</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
