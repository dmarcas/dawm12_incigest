/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package incigest.enums;


public enum UserStatus {
    
    Pendent(),
    Actiu(),
    Inactiu();
    
    public static UserStatus getUserStatus (String userStatus){
        switch (userStatus.toLowerCase()) {
               case "pendent":
                   return UserStatus.Pendent;
	       case "actiu":
		   return UserStatus.Actiu; 
	       case "inactiu":
	    	   return UserStatus.Inactiu; 
	   }
	   return null;        
    }    
}
