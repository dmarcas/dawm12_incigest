/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package incigest.enums;

public enum Status {
    
    Nova(),
    Resolucio(),
    Solucionada(),
    Tancada();
    
    public static Status getStatus (String status){
        switch (status.toLowerCase()) {
	       case "nova":
		   return Status.Nova; 
	       case "resolucio":
	    	   return Status.Resolucio; 
	       case "solucionada":
	    	   return Status.Solucionada;
               case "tancada":
                   return Status.Tancada;
	   }
	   return null;        
    }
}
