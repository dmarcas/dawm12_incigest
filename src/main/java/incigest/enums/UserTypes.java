/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package incigest.enums;


public enum UserTypes {
    
    Pendent(),
    Usuari(),
    Tecnic(),
    Admin();

    public static UserTypes getUserTypes (String userType){
        
        switch (userType.toLowerCase()) {
               case "pendent":
                   return UserTypes.Pendent;
               case "usuari":
		   return UserTypes.Usuari; 
	       case "tecnic":
	    	   return UserTypes.Tecnic; 
	       case "admin":
	    	   return UserTypes.Admin;
        }
	return null;        
    }
}
