/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package incigest.auth;

import org.springframework.security.core.Authentication;


public interface IAuthenticationFacade {
    
    Authentication getAuthentication();  
}
