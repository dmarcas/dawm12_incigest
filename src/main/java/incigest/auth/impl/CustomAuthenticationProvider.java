/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package incigest.auth.impl;

import incigest.domain.SystemUsers;
import incigest.repository.SystemUsersRepository;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;


@Component("authenticationProvider")
public class CustomAuthenticationProvider implements AuthenticationProvider {
    
    @Autowired
    private SystemUsersRepository systemUsersRepository;
    
    @Override
    public Authentication authenticate(Authentication authentication) 
            throws AuthenticationException {
        
        String name = authentication.getName();
        System.out.println("received name was: " + name);
       
        Object credentials = authentication.getCredentials();
        
        System.out.println("credentials class: " + credentials.getClass());
        
        if (!(credentials instanceof String)) {
            return null;
        }
        String password = credentials.toString();
        System.out.println("received password was: " + password);       
        
        List<SystemUsers> users = systemUsersRepository.getAllSystemUsers();
        
        Optional<SystemUsers> userOptional = Optional.empty();
        for (SystemUsers u : users){
            if (u.getUserName().equals(name) && u.getPassword().equals(password) && u.getUserStatus().equals("Actiu")){
                userOptional = Optional.of(u);
                break;
            }
        }        
        
        if (!userOptional.isPresent()) {
            throw new BadCredentialsException("Authentication failed for " + name);
        }

        List<GrantedAuthority> grantedAuthorities = new ArrayList<>();
        System.out.println("Fetched role is: " + userOptional.get().getUserTypes());
        grantedAuthorities.add(new SimpleGrantedAuthority(userOptional.get().getUserTypes()));//UserTypes.getUserTypes(userOptional.get().getUserTypes()).name()));
        Authentication auth = new
                UsernamePasswordAuthenticationToken(name, password, grantedAuthorities);
        return auth;
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(
          UsernamePasswordAuthenticationToken.class);
    }
}
