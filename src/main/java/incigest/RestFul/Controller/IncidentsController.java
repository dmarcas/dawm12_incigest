/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package incigest.RestFul.Controller;

import incigest.domain.Incident;
import incigest.service.impl.IncidentServiceImpl;
import incigest.service.impl.SystemUsersServiceImpl;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value="/ws/rest/incidents")
public class IncidentsController {
    
    @Autowired
    private IncidentServiceImpl incidentService;
    
    @Autowired
    private SystemUsersServiceImpl systemUsersService;
    
    public IncidentsController(){
        
    }
    
    public IncidentsController (IncidentServiceImpl incidentService){
        this.incidentService = incidentService;
    }
    
    @RequestMapping(value = "/byUsers/{createdBy}", method = RequestMethod.GET)
    public @ResponseBody List<Incident> getAllIncidentsByUser(@PathVariable String createdBy){        
        return this.incidentService.getIncidentByUser(systemUsersService.getUsersByName(createdBy));                      
    }
    
    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public @ResponseBody List<Incident> getAllIncidents(){        
        return this.incidentService.getAllIncidents();                     
    }
    
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public @ResponseBody Incident getIncidentById(@PathVariable String id){        
        return this.incidentService.getIncidentById(Integer.parseInt(id));                     
    }
    
    @RequestMapping(value = "/inProces", method = RequestMethod.GET)
    public @ResponseBody List<Incident> getAllIncidentsEnCurs(){        
        return this.incidentService.getAllIncidentsEnCurs();                     
    }
    
}
