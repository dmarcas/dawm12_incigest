/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package incigest.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import incigest.enums.UserStatus;
import incigest.enums.UserTypes;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


@Entity
@Table(name = "systemusers")
public class SystemUsers implements Serializable {
    
    private static final long serialVersionUID = 1L;    
    
    @Id
    @Column(name = "userName")
    @NotNull
    @Size(max = 15)
    private String userName;
    
    @Column(name = "passwordUsers")
    @NotNull
    @Size(max = 10)
    @JsonIgnore
    private String password;
    
    @Column(name = "userType")
    private String userTypes = UserTypes.Pendent.name();
    
    @Column(name = "statusUser")
    private String userStatus = UserStatus.Pendent.name();
    
    @Column(name = "department")
    private String department;

    public SystemUsers(){
    }
       
    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUserTypes() {
        return UserTypes.getUserTypes(userTypes).name();
    }

    public void setUserTypes(String userTypes) {
        this.userTypes = userTypes;
    }

    public String getUserStatus() {
        return UserStatus.getUserStatus(userStatus).name();
    }
    
    public void setUserStatus(String userStatus) {
        this.userStatus = userStatus;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }
    
    
    
}
