/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package incigest.domain;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


@Entity
@Table(name = "incidenttypes")
public class IncidentTypes implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    @Id
    @Column(name = "nameType")
    @NotNull
    @Size(max = 30)
    private String nameType;
    
    @Column(name = "descriptionType")
    @Size(max = 255)
    private String descriptionType;
    
    public IncidentTypes(){ 
    }
    
    public IncidentTypes(String name, String descriptionType) {
        this.nameType = name;
        this.descriptionType=descriptionType;
    }
    
    public String getNameType() {
        return nameType;
    }

    public void setNameType(String nameType) {
        this.nameType = nameType;
    }

    public String getDescriptionType() {
        return descriptionType;
    }

    public void setDescriptionType(String descriptionType) {
        this.descriptionType = descriptionType;
    }
}
