/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package incigest.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.io.Serializable;
import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.hibernate.annotations.CreationTimestamp;


@Entity
@Table(name = "incident")
public class Incident implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "incidentId")
    @NotNull    
    private int incidentId;
    
    @Column(name = "title")
    @NotNull    
    @Size(max = 30)
    private String title;
     
    @Column(name = "creationDate")  
    @CreationTimestamp
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd@HH:mm:ss.SSSZ")
    private Timestamp creationDate;

    @Column(name = "endDate")    
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd@HH:mm:ss.SSSZ")
    private Timestamp endDate;

    @ManyToOne
    @JoinColumn (name="createdBy")
    private SystemUsers createdBy;
    
    @ManyToOne
    @NotNull
    @JoinColumn (name="typeIncident")
    private IncidentTypes type;
    
    @Column(name = "statusIncident")
    @NotNull    
    private String status;
    
    @Column(name = "descriptionIncident")
    @NotNull    
    @Size(max = 255)
    private String description;

    @ManyToOne
    @JoinColumn (name="technicAssigned")
    private SystemUsers technicAssigned;
    
    public Incident(){        
    } 
    
    public int getIncidentId() {
        return incidentId;
    }

    public void setIncidentId(int incidentId) {
        this.incidentId = incidentId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Timestamp getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Timestamp creationDate) {
        this.creationDate = creationDate;
    }

    public Timestamp getEndDate() {
        return endDate;
    }

    public void setEndDate(Timestamp endDate) {
        this.endDate = endDate;
    }

    public SystemUsers getCreatedBy() {
        return createdBy;
    }
    
    public void setCreatedBy(SystemUsers createdBy) {
        this.createdBy = createdBy;
    }
    
    public IncidentTypes getType() {
        return type;
    }
    
    public void setType(IncidentTypes type) {
        this.type = type;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public SystemUsers getTechnicAssigned() {
        return technicAssigned;
    }

    public void setTechnicAssigned(SystemUsers technicAssigned) {
        this.technicAssigned = technicAssigned;
    }
    
}
