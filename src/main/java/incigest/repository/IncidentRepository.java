package incigest.repository;

import incigest.domain.Incident;
import incigest.domain.SystemUsers;
import java.sql.Timestamp;
import java.util.List;



public interface IncidentRepository {
    
    Incident getIncidentById(int incidentId);

    List<Incident> getIncidentByUser(SystemUsers createdBy);
    
    List <Incident> getAllIncidents();
    
    List <Incident> getIncidentByStatus(String status);
    
    List<Incident> getSearchIncidentsByUser(Incident incidentCriteria, SystemUsers createdBy);
    
    List<Incident> getSearchIncidents(Incident incidentCriteria);
    
    List <Incident> getIncidentsByTitle (String title);
    
    List <Incident> getIncidentsByType (String typeIncident);
    
    List <Incident> getIncidentsByCreationDate (Timestamp creationDate);
    
    List <Incident> getIncidentsByTechnic (SystemUsers technicAssigned);     
    
    void addIncident(Incident incident);
    
    void deleteIncident(Incident incident);
    
    void updateIncident(Incident incident);
}
