/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package incigest.repository.impl;

import incigest.domain.SystemUsers;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import incigest.repository.SystemUsersRepository;
import org.hibernate.criterion.Restrictions;


@Transactional
@Repository
public class SystemUsersHibernate implements SystemUsersRepository {

    @Autowired
    private SessionFactory sessionFactory;
    
    @Override
    public SystemUsers getUsersByName(String userName) {
        return getSession().get(SystemUsers.class, userName);        
    }    
    
    @Override
    public void addSystemUsers(SystemUsers systemUser) {
        getSession().saveOrUpdate(systemUser);
    }
    
    @Override
    public void updateSystemUsers(SystemUsers systemUsers) {
        getSession().merge(systemUsers);
    }

    @Override
    public List<SystemUsers> getAllSystemUsers() {       
        Criteria criteria = createEntityCriteria();
        return  (List<SystemUsers>) criteria.list();
    }
    
    @Override
    public void deleteSystemUsers(SystemUsers systemUsers) {
        getSession().delete(systemUsers);
    }
    
    @Override
    public List<SystemUsers> getSystemUsersPendingConfirmation(String userStatus) {
        Criteria criteria = createEntityCriteria();
        criteria.add(Restrictions.eq("userStatus", userStatus));
        return criteria.list();
    
    }
    
    @Override
    public List<SystemUsers> getSystemUsersByUserType(String userType) {
        Criteria criteria = createEntityCriteria();
        criteria.add(Restrictions.eq("userTypes", userType));
        return criteria.list();
    
    }

    private Session getSession() {
        return sessionFactory.getCurrentSession();
    }

    private Criteria createEntityCriteria() {
        return getSession().createCriteria(SystemUsers.class);
    }
}
