/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package incigest.repository.impl;

import incigest.domain.Incident;
import incigest.domain.SystemUsers;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import incigest.repository.IncidentRepository;
import org.hibernate.criterion.Conjunction;
import org.hibernate.criterion.Restrictions;
import java.sql.Timestamp;

@Transactional
@Repository
public class IncidentHibernate implements IncidentRepository {

    @Autowired
    private SessionFactory sessionFactory;
    
    @Override
    public Incident getIncidentById(int incidentId) {
        return getSession().get(Incident.class, incidentId);        
    }
    
    @Override
    public List<Incident> getIncidentByUser(SystemUsers createdBy) {
        Criteria criteria = createEntityCriteria();
        criteria.add(Restrictions.eq("createdBy", createdBy));
        return (List<Incident>) criteria.list();
    }     
  
    @Override
    public List<Incident> getAllIncidents(){        
        Criteria criteria = createEntityCriteria();
        return (List<Incident>) criteria.list();
    }
    
    @Override
    public List<Incident> getIncidentByStatus(String status){
        Criteria criteria = createEntityCriteria();
        criteria.add(Restrictions.eq("status", status));
        return (List<Incident>) criteria.list();
    }
    
    @Override
    public void addIncident(Incident incident) {
        getSession().saveOrUpdate(incident);
    }
    
    @Override
    public void deleteIncident(Incident incident){
        getSession().delete(incident);
    }

    @Override
    public void updateIncident(Incident incident) {
        getSession().merge(incident);
    }

    private Session getSession() {
        return sessionFactory.getCurrentSession();
    }

    private Criteria createEntityCriteria() {
        return getSession().createCriteria(Incident.class);
    }

    @Override
    public List<Incident> getIncidentsByTitle(String title) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<Incident> getIncidentsByType(String typeIncident) {
        Criteria criteria = createEntityCriteria();
        criteria.add(Restrictions.eq("type", typeIncident));
        return (List<Incident>) criteria.list();
    }

    @Override
    public List<Incident> getIncidentsByCreationDate(Timestamp creationDate) {       
        Timestamp endCreationDateDay = creationDate;
        endCreationDateDay.setHours(23);
        endCreationDateDay.setMinutes(59);
        endCreationDateDay.setSeconds(59);
        Criteria criteria = createEntityCriteria();
        criteria.add(Restrictions.between("creationDate", creationDate, endCreationDateDay));
        return (List<Incident>) criteria.list();
    }    
   
    @Override
    public List<Incident> getSearchIncidents(Incident incidentCriteria){
        Timestamp endCreationDateDay = new Timestamp(System.currentTimeMillis());
        Timestamp endCreationEndDateDay = new Timestamp(System.currentTimeMillis());
        if (incidentCriteria.getCreationDate()!=null){
            endCreationDateDay.setYear(incidentCriteria.getCreationDate().getYear());
            endCreationDateDay.setMonth(incidentCriteria.getCreationDate().getMonth());
            endCreationDateDay.setDate(incidentCriteria.getCreationDate().getDate());
            endCreationDateDay.setHours(23);
            endCreationDateDay.setMinutes(59);
            endCreationDateDay.setSeconds(59);
        }
        if (incidentCriteria.getEndDate()!=null){
            endCreationEndDateDay.setYear(incidentCriteria.getEndDate().getYear());
            endCreationEndDateDay.setMonth(incidentCriteria.getEndDate().getMonth());
            endCreationEndDateDay.setDate(incidentCriteria.getEndDate().getDate());
            endCreationEndDateDay.setHours(23);
            endCreationEndDateDay.setMinutes(59);
            endCreationEndDateDay.setSeconds(59);
        }
        Criteria criteria = createEntityCriteria();
        Conjunction and = Restrictions.conjunction();
        if (!incidentCriteria.getTitle().isEmpty()){
            and.add(Restrictions.eq("title", incidentCriteria.getTitle()));
        }        
        if (!incidentCriteria.getCreatedBy().getUserName().equals("null")){
            and.add(Restrictions.eq("createdBy", incidentCriteria.getCreatedBy()));
        }        
        if (!incidentCriteria.getType().getNameType().equals("null")){
            and.add(Restrictions.eq("type", incidentCriteria.getType()));
        }
        if (!incidentCriteria.getTechnicAssigned().getUserName().equals("null")){
            and.add(Restrictions.eq("technicAssigned", incidentCriteria.getTechnicAssigned()));
        }        
        if (!incidentCriteria.getStatus().equals("test")){
            and.add(Restrictions.eq("status", incidentCriteria.getStatus()));
        }
        if ((incidentCriteria.getCreationDate()!=null)&&(incidentCriteria.getEndDate()==null)){
            and.add(Restrictions.between("creationDate", incidentCriteria.getCreationDate(), endCreationDateDay));
        }        
        if ((incidentCriteria.getCreationDate()!=null)&&(incidentCriteria.getEndDate()!=null)){
            Conjunction objConjunction = Restrictions.conjunction();
            objConjunction.add(Restrictions.ge("creationDate", incidentCriteria.getCreationDate()));
            objConjunction.add(Restrictions.le("creationDate", incidentCriteria.getEndDate()));
            and.add(objConjunction);
        }        
        criteria.add(and);
        return (List<Incident>) criteria.list();
    }
    
    @Override
    public List<Incident> getIncidentsByTechnic(SystemUsers technicAssigned) {
        Criteria criteria = createEntityCriteria();
        criteria.add(Restrictions.eq("technicAssigned", technicAssigned));
        return (List<Incident>) criteria.list();
    }
    
    @Override
    public List<Incident> getSearchIncidentsByUser(Incident incidentCriteria, SystemUsers createdBy){
        Timestamp endCreationDateDay = new Timestamp(System.currentTimeMillis());
        Timestamp endCreationEndDateDay = new Timestamp(System.currentTimeMillis());
        if (incidentCriteria.getCreationDate()!=null){
            endCreationDateDay.setYear(incidentCriteria.getCreationDate().getYear());
            endCreationDateDay.setMonth(incidentCriteria.getCreationDate().getMonth());
            endCreationDateDay.setDate(incidentCriteria.getCreationDate().getDate());
            endCreationDateDay.setHours(23);
            endCreationDateDay.setMinutes(59);
            endCreationDateDay.setSeconds(59);
        } 
        if (incidentCriteria.getEndDate()!=null){
            endCreationEndDateDay.setYear(incidentCriteria.getEndDate().getYear());
            endCreationEndDateDay.setMonth(incidentCriteria.getEndDate().getMonth());
            endCreationEndDateDay.setDate(incidentCriteria.getEndDate().getDate());
            endCreationEndDateDay.setHours(23);
            endCreationEndDateDay.setMinutes(59);
            endCreationEndDateDay.setSeconds(59);
        } 
        
        Criteria criteria = createEntityCriteria();
        Conjunction addConjunction=Restrictions.conjunction();
        addConjunction.add(Restrictions.eq("createdBy", createdBy));
        if (!incidentCriteria.getTitle().isEmpty()){
            addConjunction.add(Restrictions.eq("title", incidentCriteria.getTitle()));
        }
        if (!incidentCriteria.getType().getNameType().equals("null")){
            addConjunction.add(Restrictions.eq("type", incidentCriteria.getType()));
        } 
        if (!incidentCriteria.getTechnicAssigned().getUserName().equals("null")){
            addConjunction.add(Restrictions.eq("technicAssigned", incidentCriteria.getTechnicAssigned()));
        }      
        if (!incidentCriteria.getStatus().equals("test")){
            addConjunction.add(Restrictions.eq("status", incidentCriteria.getStatus())); 
        }
        if ((incidentCriteria.getCreationDate()!=null)&&(incidentCriteria.getEndDate()==null)){
            addConjunction.add(Restrictions.between("creationDate", incidentCriteria.getCreationDate(), endCreationDateDay ));
        }        
        if ((incidentCriteria.getCreationDate()!=null)&&(incidentCriteria.getEndDate()!=null)){
            Conjunction objConjunction = Restrictions.conjunction();
            objConjunction.add(Restrictions.ge("creationDate", incidentCriteria.getCreationDate()));
            objConjunction.add(Restrictions.le("creationDate", incidentCriteria.getEndDate()));
            addConjunction.add(objConjunction);
        }
        criteria.add(addConjunction);
        return (List<Incident>) criteria.list();
    }
}
