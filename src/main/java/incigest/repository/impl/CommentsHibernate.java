/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package incigest.repository.impl;

import incigest.domain.Comments;
import incigest.domain.Incident;
import incigest.repository.CommentsRepository;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;



@Transactional
@Repository
public class CommentsHibernate implements CommentsRepository {

    @Autowired
    private SessionFactory sessionFactory;
    
    @Override
    public Comments getCommentById (int commentId) {
        return getSession().get(Comments.class, commentId);        
    }
    
    @Override
    public void addComment(Comments comment) {
        getSession().saveOrUpdate(comment);
    }

    @Override
    public List<Comments> getAllComments() {
        Criteria criteria = createEntityCriteria();
        return  (List<Comments>) criteria.list();
    }
    
    @Override
    public List<Comments> getCommentsByIncidenceId(Incident incidentId) {
        Criteria criteria = createEntityCriteria();
        criteria.add(Restrictions.eq("incidentId", incidentId));
        return (List<Comments>) criteria.list();
    }
    
    @Override
    public void deleteComment (Comments comment) {
        getSession().delete(comment);
    }

    private Session getSession() {
        return sessionFactory.getCurrentSession();
    }

    private Criteria createEntityCriteria() {
        return getSession().createCriteria(Comments.class);
    }   
}
