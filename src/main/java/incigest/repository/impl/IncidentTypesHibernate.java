/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package incigest.repository.impl;

import incigest.domain.IncidentTypes;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import incigest.repository.IncidentTypesRepository;


@Transactional
@Repository
public class IncidentTypesHibernate implements IncidentTypesRepository {

    @Autowired
    private SessionFactory sessionFactory;
    
    @Override
    public IncidentTypes getIncidentTypesByName(String nameType) {
        return getSession().get(IncidentTypes.class, nameType);        
    }
    
    @Override
    public void addIncidentTypes(IncidentTypes incidentType){
        getSession().saveOrUpdate(incidentType);
    }

    @Override
    public List<IncidentTypes> getAllIncidentTypes() {
        Criteria criteria = createEntityCriteria();
        return  (List<IncidentTypes>) criteria.list();
    }
    
    @Override
    public void removeIncidentTypes (IncidentTypes incidentTypes){
        getSession().delete(incidentTypes);
    }

    @Override
    public void updateIncidentTypes(IncidentTypes incidentTypes) {
        getSession().merge(incidentTypes);
    }

    private Session getSession() {
        return sessionFactory.getCurrentSession();
    }

    private Criteria createEntityCriteria() {
        return getSession().createCriteria(IncidentTypes.class);
    }
}
