package incigest.repository;

import incigest.domain.IncidentTypes;
import java.util.List;

public interface IncidentTypesRepository {
    
    IncidentTypes getIncidentTypesByName(String nameType);
    
    void addIncidentTypes(IncidentTypes incidentType);
    
    List<IncidentTypes> getAllIncidentTypes();  
    
    void updateIncidentTypes (IncidentTypes incidentTypes);
    
    void removeIncidentTypes (IncidentTypes incidentTypes);
}
