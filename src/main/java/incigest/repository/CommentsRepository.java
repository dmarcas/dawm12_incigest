package incigest.repository;

import incigest.domain.Comments;
import incigest.domain.Incident;
import java.util.List;

public interface CommentsRepository {
    
    Comments getCommentById(int commentId);
    
    List<Comments> getAllComments();  

    List<Comments> getCommentsByIncidenceId(Incident incidentId);
    
    void addComment(Comments comment);
    
    void deleteComment (Comments comment);
    
    
}
