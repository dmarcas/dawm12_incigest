package incigest.repository;

import incigest.domain.SystemUsers;
import java.util.List;

public interface SystemUsersRepository {
    
    SystemUsers getUsersByName(String userName);
    
    List<SystemUsers> getAllSystemUsers();  

    List<SystemUsers> getSystemUsersPendingConfirmation(String userStatus);
    
    void addSystemUsers(SystemUsers systemUser);
    
    void updateSystemUsers(SystemUsers systemUsers);
    
    void deleteSystemUsers(SystemUsers systemUsers);
    
    List<SystemUsers> getSystemUsersByUserType(String userType);
    
}
