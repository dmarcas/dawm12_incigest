package incigest.service;

import incigest.domain.IncidentTypes;
import java.util.List;

public interface IncidentTypesService {
    
    IncidentTypes getIncidentTypesByName(String name);   
   
    List<IncidentTypes> getAllIncidentTypes();

    List<String> getListNamesIncidentTypes();
        
    void removeIncidentTypes (IncidentTypes incidentTypes);
    
    void addIncidentTypes(IncidentTypes incidentType);
  
    void updateIncidentTypes(IncidentTypes incidentTypes);
}
