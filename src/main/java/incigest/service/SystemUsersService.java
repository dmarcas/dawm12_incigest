package incigest.service;

import incigest.domain.SystemUsers;
import java.util.List;

public interface SystemUsersService {
    
    SystemUsers getUsersByName(String userName);
    
    List<SystemUsers> getAllSystemUsers();
    
    List<SystemUsers> getAllTechnics();

    List<SystemUsers> getSystemUsersPendingConfirmation(String userStatus);

    List<String> getTechnicsNames();
    
    void addSystemUsers(SystemUsers systemUser);
    
    void updateSystemUsers(SystemUsers systemUsers);
    
    void deleteSystemUsers(SystemUsers systemUsers);
    
    List<SystemUsers> getSystemUsersByUserType(String userType);
    
}
