package incigest.service;

import incigest.domain.Comments;
import incigest.domain.Incident;
import java.util.List;

public interface CommentsService {
    
    Comments getCommentById(int commentId);
    
    List<Comments> getAllComments();  
    
    void addComment(Comments comment);
    
    List<Comments> getCommentsByIncidenceId(Incident incidentId);
    
    void deleteComment (Comments comment);
}
