package incigest.service;

import incigest.domain.Incident;
import incigest.domain.SystemUsers;
import incigest.enums.Status;
import java.util.List;


public interface IncidentService {

    Incident getIncidentById(int incidentId);

    List<Incident> getIncidentByUser(SystemUsers createdBy);

    List<Incident> getIncidentNewByUser(SystemUsers createdBy);
    
    List<Incident> getIncidentsEnCursByUser(SystemUsers createdBy);
    
    List<Incident> getIncidentsSolucionadaByUser(SystemUsers createdBy);
    
    List<Incident> getIncidentsTancatsByUser(SystemUsers createdBy);
    
    List<Status> getAllStatus();

    List<Incident> getAllIncidents();
    
    List<Incident> getAllIncidentsEnCurs();
    
    List<Incident> getIncidentByStatus(String status);
    
    List<Incident> getSearchIncidentsByUser(Incident incidentCriteria, SystemUsers createdBy);
    
    List<Incident> getSearchIncidents(Incident incidentCriteria);
    
    List<Incident> getIncidentByTechnic(SystemUsers technicAssigned);

    List<Incident> getIncidentNewByTechnic(SystemUsers technicAssigned);
    
    List<Incident> getIncidentsEnCursByTechnic(SystemUsers technicAssigned);
    
    List<Incident> getIncidentsSolucionadaByTechnic(SystemUsers technicAssigned);
    
    List<Incident> getIncidentsTancatsByTechnic(SystemUsers technicAssigned);

    void addIncident(Incident incident);
    
    void deleteIncident(Incident incident);
    
    void updateIncident(Incident incident);
    
    void closedIncident(int incidentId);
    
    void openIncident(int incidentId);
    
}
