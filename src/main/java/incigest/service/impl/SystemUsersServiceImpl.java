/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package incigest.service.impl;

import incigest.auth.IAuthenticationFacade;
import incigest.domain.SystemUsers;
import incigest.repository.SystemUsersRepository;
import incigest.service.SystemUsersService;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;


@Service
public class SystemUsersServiceImpl implements SystemUsersService{
    
    @Autowired
    private IAuthenticationFacade authenticationFacade;
    
    @Autowired
    private SystemUsersRepository systemUsersRepository;     


    @Override
    public SystemUsers getUsersByName(String userName) {      
        return systemUsersRepository.getUsersByName(userName);
    }

    @Override
    public List<SystemUsers> getAllSystemUsers() {
        Authentication authentication = authenticationFacade.getAuthentication();
        System.out.println("INFO AT SERVICE LAYER, auth user was: " + authentication.getName());
        return systemUsersRepository.getAllSystemUsers();
    }
    
    @Override
    public List<SystemUsers> getSystemUsersPendingConfirmation(String userStatus) {
        return systemUsersRepository.getSystemUsersPendingConfirmation(userStatus);
    }

    
    @Override
    public List<SystemUsers> getAllTechnics(){
        List<SystemUsers> technics = new ArrayList();
        for (SystemUsers usuaris: systemUsersRepository.getAllSystemUsers()){
            if (usuaris.getUserTypes().equals("Tecnic")){
                technics.add(usuaris);
            }
        }
        return technics;
    }
    
    @Override
    public List<String> getTechnicsNames(){
        List<String> tecnics = new ArrayList();
        for (SystemUsers tecnic : systemUsersRepository.getSystemUsersByUserType("Tecnic")){
            tecnics.add(tecnic.getUserName());
        }        
        return tecnics;
    }
    
    @Override
    public void addSystemUsers(SystemUsers systemUsers) {        
        systemUsersRepository.addSystemUsers(systemUsers);
    }
    
    @Override
    public void updateSystemUsers(SystemUsers systemUsers) {
        systemUsersRepository.updateSystemUsers(systemUsers);
    }
    
    @Override
    public void deleteSystemUsers(SystemUsers systemUsers) {
        systemUsersRepository.deleteSystemUsers(systemUsers);
    }    
    
    @Override
    public List<SystemUsers> getSystemUsersByUserType(String userType){
        return systemUsersRepository.getSystemUsersByUserType(userType);
    }
}
