/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package incigest.service.impl;


import incigest.domain.Incident;
import incigest.domain.SystemUsers;
import incigest.enums.Status;
import incigest.repository.IncidentRepository;
import incigest.repository.SystemUsersRepository;
import incigest.service.IncidentService;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class IncidentServiceImpl implements IncidentService{
    
    @Autowired
    private IncidentRepository incidentRepository;
    
    @Autowired
    private SystemUsersRepository systemUsersRepository;

    @Override
    public Incident getIncidentById(int incidentId) {        
        return incidentRepository.getIncidentById(incidentId);
    }

    @Override
    public List<Incident> getIncidentByUser(SystemUsers createdBy) {
        return incidentRepository.getIncidentByUser(createdBy);
    }
        
    
    @Override
    public List<Incident> getIncidentNewByUser(SystemUsers createdBy) {
        List<Incident> incidentsNew = new ArrayList();
        for (Incident incidents: incidentRepository.getIncidentByUser(createdBy)){
            if(incidents.getStatus().equals("Nova")){
                incidentsNew.add(incidents);
            }
        }
        return incidentsNew;
    }
    
    @Override
    public List<Incident> getIncidentsEnCursByUser(SystemUsers createdBy) {
        List<Incident> incidentsEnCurs = new ArrayList();
        for(Incident incidents: incidentRepository.getIncidentByUser(createdBy)){
            if(incidents.getStatus().equals("Resolucio") || incidents.getStatus().equals("Solucionada")){
                  incidentsEnCurs.add(incidents);
            }
        }
        return incidentsEnCurs;
    }
    
    @Override
    public List<Incident> getIncidentsSolucionadaByUser(SystemUsers createdBy) {
        List<Incident> incidentsSolucionats = new ArrayList();
        for (Incident incidents: incidentRepository.getIncidentByUser(createdBy)){
            if(incidents.getStatus().equals("Solucionada")){
                incidentsSolucionats.add(incidents);
            }
        }
        return incidentsSolucionats;
    }
    
    @Override
    public List<Incident> getIncidentsTancatsByUser(SystemUsers createdBy) {
        List<Incident> incidentsTancats = new ArrayList();
        for (Incident incidents: incidentRepository.getIncidentByUser(createdBy)){
            if(incidents.getStatus().equals("Tancada")){
                incidentsTancats.add(incidents);
            }
        }
        return incidentsTancats;
    }
  
    @Override
    public List<Incident> getAllIncidents(){
        return incidentRepository.getAllIncidents();
    }
    
    @Override
    public List<Incident> getIncidentByStatus(String status){
        return incidentRepository.getIncidentByStatus(status);
    }
       
    @Override
    public List<Incident> getAllIncidentsEnCurs() {
        List<Incident> incidentsEnCurs = new ArrayList();
        for(Incident incidents: incidentRepository.getAllIncidents()){
            if(!(incidents.getStatus().equals(Status.Tancada.name()))&&(!incidents.getStatus().equals(Status.Nova.name()))){
                  incidentsEnCurs.add(incidents);
            }
        }
        return incidentsEnCurs;
    }
    
    @Override
    public List<Status> getAllStatus(){
        List<Status> statusList = new ArrayList<>(Arrays.asList(Status.values()));
        return statusList;
    }
    
    @Override
    public List<Incident> getIncidentByTechnic(SystemUsers technicAssigned){
        return incidentRepository.getIncidentsByTechnic(technicAssigned);
    }

    @Override
    public List<Incident> getIncidentNewByTechnic(SystemUsers technicAssigned){
        List<Incident> incidentsNous = new ArrayList();
        for (Incident incidents: incidentRepository.getIncidentsByTechnic(technicAssigned)){
            if(incidents.getStatus().equals("Nova")){
                incidentsNous.add(incidents);
            }
        }
        return incidentsNous; 
    }
    
        
    @Override
    public List<Incident> getIncidentsEnCursByTechnic(SystemUsers technicAssigned){
        List<Incident> incidentsEnCurs = new ArrayList();
        for (Incident incidents: incidentRepository.getIncidentsByTechnic(technicAssigned)){
            if(incidents.getStatus().equals("Resolucio")){
                incidentsEnCurs.add(incidents);
            }
        }
        return incidentsEnCurs; 
    }
    
    @Override
    public List<Incident> getIncidentsSolucionadaByTechnic(SystemUsers technicAssigned){
        List<Incident> incidentsSolucionada = new ArrayList();
        for (Incident incidents: incidentRepository.getIncidentsByTechnic(technicAssigned)){
            if(incidents.getStatus().equals("Solucionada")){
                incidentsSolucionada.add(incidents);
            }
        }
        return incidentsSolucionada; 
    }
    
    @Override
    public List<Incident> getIncidentsTancatsByTechnic(SystemUsers technicAssigned){
        List<Incident> incidentsTancats = new ArrayList();
        for (Incident incidents: incidentRepository.getIncidentsByTechnic(technicAssigned)){
            if(incidents.getStatus().equals("Tancada")){
                incidentsTancats.add(incidents);
            }
        }
        return incidentsTancats; 
    }

    @Override
    public void addIncident(Incident incident) {
        if(incident.getStatus() == null){
            incident.setStatus(Status.Nova.name());
            incident.setTechnicAssigned(systemUsersRepository.getUsersByName("Pendent"));
        }
        incidentRepository.addIncident(incident);
    }
    
    @Override
    public void deleteIncident(Incident incident){
        incidentRepository.deleteIncident(incident);
    }
    
    @Override
    public void updateIncident(Incident incident) {
        incidentRepository.updateIncident(incident);
    }
    
    @Override
    public void closedIncident(int incidentId){
        Incident incident = incidentRepository.getIncidentById(incidentId);
        incident.setStatus("Tancada");
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        incident.setEndDate(timestamp);
        incidentRepository.updateIncident(incident);
    }
    
    @Override
    public void openIncident(int incidentId){
        Incident incident = incidentRepository.getIncidentById(incidentId);
        incident.setStatus("Resolucio");
        incidentRepository.updateIncident(incident);
    }
    
    
    @Override
    public List<Incident> getSearchIncidents(Incident incidentCriteria) {
        return incidentRepository.getSearchIncidents(incidentCriteria);
    }
    
    @Override
    public List<Incident> getSearchIncidentsByUser(Incident incidentCriteria, SystemUsers createdBy){
        return incidentRepository.getSearchIncidentsByUser(incidentCriteria, createdBy);
    }  
}
