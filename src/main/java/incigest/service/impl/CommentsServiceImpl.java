/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package incigest.service.impl;

import incigest.domain.Comments;
import incigest.domain.Incident;
import incigest.repository.CommentsRepository;
import incigest.service.CommentsService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class CommentsServiceImpl implements CommentsService{
    
    @Autowired
    private CommentsRepository commentsRepository;     


    @Override
    public Comments getCommentById(int commentId) {      
        return commentsRepository.getCommentById(commentId);
    }

    @Override
    public void addComment(Comments comment) {    
        commentsRepository.addComment(comment);
    }
    
    @Override
    public List<Comments> getAllComments() {
        return commentsRepository.getAllComments();           
    }
    
    @Override
    public List<Comments> getCommentsByIncidenceId(Incident incidentId){
        return commentsRepository.getCommentsByIncidenceId(incidentId);
    }
    
    @Override
    public void deleteComment (Comments comment) {
        commentsRepository.deleteComment(comment);
    }
        
}
