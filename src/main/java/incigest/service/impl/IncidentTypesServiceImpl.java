/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package incigest.service.impl;

import incigest.domain.IncidentTypes;
import incigest.repository.IncidentTypesRepository;
import incigest.service.IncidentTypesService;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class IncidentTypesServiceImpl implements IncidentTypesService{
    
    @Autowired
    private IncidentTypesRepository incidentTypesRepository;     


    @Override
    public IncidentTypes getIncidentTypesByName(String nameType) {        
        return incidentTypesRepository.getIncidentTypesByName(nameType);
    }

    @Override
    public void addIncidentTypes(IncidentTypes incidentTypes) {        
        incidentTypesRepository.addIncidentTypes(incidentTypes);
    }
    
    @Override
    public List<IncidentTypes> getAllIncidentTypes() {
        return incidentTypesRepository.getAllIncidentTypes();
    }
    
    @Override
    public void removeIncidentTypes (IncidentTypes incidentTypes){
        incidentTypesRepository.removeIncidentTypes(incidentTypes);
    }

    @Override
    public void updateIncidentTypes(IncidentTypes incidentTypes) {
        incidentTypesRepository.updateIncidentTypes(incidentTypes);
    }
    
    @Override
    public List<String> getListNamesIncidentTypes(){
        List<String> incidents = new ArrayList();
        for (IncidentTypes incident : incidentTypesRepository.getAllIncidentTypes()){
            incidents.add(incident.getNameType());
        }
        return incidents;
    }
}
