/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package incigest.controller;

import incigest.service.SystemUsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


@Controller
public class LoginController {
    
    @Autowired
    SystemUsersService systemUsersService;
       
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String login(Model model){
        model.addAttribute("benvinguda", "Incigest");
        model.addAttribute("tagline", "Una aplicació per Gestionar les teves incidències");
        return "login";
    }
    
    @RequestMapping(value = "/loginfailed", method = RequestMethod.GET)
    public String loginerror(Model model) {
        model.addAttribute("benvinguda", "InciGest");
        model.addAttribute("tagline", "Una aplicació per Gestionar les teves incidències");
        model.addAttribute("error", "true");
        return "login";
    }
}