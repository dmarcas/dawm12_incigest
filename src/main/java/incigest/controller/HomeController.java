package incigest.controller;

import incigest.auth.IAuthenticationFacade;
import incigest.enums.UserTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class HomeController {
        
    @Autowired
    private IAuthenticationFacade authenticationFacade;
   
    @RequestMapping(value = "/home", method = RequestMethod.GET)
    public String redirectRoles(){       
        String redirection = "redirect:/";
        Authentication authentication = authenticationFacade.getAuthentication();
        for (GrantedAuthority auth : authentication.getAuthorities()){
            if (auth.getAuthority().equals(UserTypes.getUserTypes("Admin").name())){
                redirection ="redirect:/home/adminHome";                
            } else if (auth.getAuthority().equals(UserTypes.getUserTypes("Tecnic").name())){
                redirection ="redirect:/home/technicalHome";   
            } else if (auth.getAuthority().equals(UserTypes.getUserTypes("Usuari").name())){
                System.out.println("test");
                redirection ="redirect:/home/userHome";   
            }
        }
        return redirection;
    }  
}
