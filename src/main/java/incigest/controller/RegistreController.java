package incigest.controller;

import incigest.domain.SystemUsers;
import incigest.service.SystemUsersService;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;


@Controller
public class RegistreController {

    @Autowired
    SystemUsersService systemUsersService;
    
    @InitBinder
    public void initialiseBinder(WebDataBinder binder) {
        binder.setDisallowedFields("userTypes");
        binder.setDisallowedFields("userStatus");
    }
    
    @RequestMapping(value = "/registre", method = RequestMethod.GET)
    public ModelAndView viewRgistre(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        ModelAndView modelview = new ModelAndView("registre");
        modelview.getModelMap().addAttribute("benvinguda", "Registra't a InciGest!");
        SystemUsers formRegistre = null;
        formRegistre = new SystemUsers();
        modelview.getModelMap().addAttribute("formRegistre", formRegistre);
        return modelview;  
    }
    
    @RequestMapping(value = "/registre/add", method = RequestMethod.POST)
    public String processRegistreForm(@ModelAttribute("formRegistre") SystemUsers formRegistre, BindingResult result) {
        String ret = "redirect:/";
        SystemUsers nomBBDD=systemUsersService.getUsersByName(formRegistre.getUserName());
        if(formRegistre.getUserName().isEmpty() || formRegistre.getPassword().isEmpty()){
            ret = "redirect:/registreIncorrecte";            
        }else if ((nomBBDD!=null)&&(systemUsersService.getUsersByName(formRegistre.getUserName()).getUserName()==(formRegistre.getUserName()))){
            ret = "redirect:/invaliduser";
        }else {
            systemUsersService.updateSystemUsers(formRegistre);
            ret = "redirect:/registrat";            
        }        
        return  ret;
    }
    
    @RequestMapping(value = "/registreIncorrecte", method = RequestMethod.GET)
    public String incorrectRegister(@ModelAttribute("formRegistre") SystemUsers formRegistre, BindingResult result, Model model) {
        model.addAttribute("benvinguda", "Registra't a InciGest!");
        model.addAttribute("tagline", "Una aplicació per Gestionar les teves incidències");
        model.addAttribute("incorrectregister", "REGISTRE INCORRECTE: Ompli tots els camps correctament i premi registrar");
        model.addAttribute("alertType", "alert alert-danger");
        model.addAttribute("error", "true");
        return "registre";
    }
    
    @RequestMapping(value = "/invaliduser", method = RequestMethod.GET)
    public String invalidUser(@ModelAttribute("formRegistre") SystemUsers formRegistre, BindingResult result, Model model) {
        model.addAttribute("benvinguda", "Registra't a InciGest!");
        model.addAttribute("tagline", "Una aplicació per Gestionar les teves incidències");
        model.addAttribute("incorrectregister", "REGISTRE INCORRECTE: L'usari escollit no està disponible, seleccioni un altre i premi registrar");
        model.addAttribute("alertType", "alert alert-danger");
        model.addAttribute("error", "true");
        return "registre";
    }
    
    @RequestMapping(value = "/registrat", method = RequestMethod.GET)
    public String registrat(@ModelAttribute("formRegistre") SystemUsers formRegistre, BindingResult result, Model model) {
        model.addAttribute("benvinguda", "Registra't a InciGest!");
        model.addAttribute("tagline", "Una aplicació per Gestionar les teves incidències");
        model.addAttribute("incorrectregister", "REGISTRE CORRECTE: El seu compte esta pendent d'activar per l'administrador");
        model.addAttribute("error", "true");
        model.addAttribute("alertType", "alert alert-success");
        return "registre";
    }
}
