/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package incigest.controller;

import incigest.auth.IAuthenticationFacade;
import incigest.domain.Comments;
import incigest.domain.Incident;
import incigest.service.CommentsService;
import incigest.service.IncidentService;
import incigest.service.IncidentTypesService;
import incigest.service.SystemUsersService;
import java.io.IOException;
import java.sql.Timestamp;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;


@Controller
@RequestMapping(value = "/home/incidents")
public class IncidentController {
    
    @Autowired
    SystemUsersService systemUsersService;
    
    @Autowired
    private IAuthenticationFacade authenticationFacade;
    
    @Autowired
    IncidentService incidentService;
    
    @Autowired
    IncidentTypesService incidentTypesService;
    
    @Autowired
    CommentsService commentsService;
    
    
    @InitBinder
    public void initialiseBinder(WebDataBinder binder) {
        binder.setDisallowedFields("incidentID");
        binder.setDisallowedFields("creationDate");
        binder.setDisallowedFields("endDate");
        binder.setDisallowedFields("comments");
    }
    
    @RequestMapping(value = "", method = RequestMethod.GET)
    public ModelAndView incident(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
                
        ModelAndView modelview = new ModelAndView("incidents");
        Authentication authentication = authenticationFacade.getAuthentication();

        modelview.getModelMap().addAttribute("userLogged", authentication.getPrincipal());  

        String nameAuth = systemUsersService.getUsersByName(authentication.getName()).getUserTypes();
        modelview.getModelMap().addAttribute("userAuthority", nameAuth);
        
        if ( nameAuth.equals("Usuari")){
            modelview.getModelMap().addAttribute("incidenciesNoves", incidentService.getIncidentNewByUser(systemUsersService.getUsersByName(authentication.getName())));
            modelview.getModelMap().addAttribute("incidencies", incidentService.getIncidentsEnCursByUser(systemUsersService.getUsersByName(authentication.getName())));
        } else {
            modelview.getModelMap().addAttribute("incidenciesNoves", incidentService.getIncidentByStatus("Nova"));
            modelview.getModelMap().addAttribute("incidencies", incidentService.getAllIncidentsEnCurs());
        }
        return modelview;
    }
    
    @RequestMapping(value = "/viewIncident", method = RequestMethod.GET)
    public ModelAndView viewIncident(@RequestParam("incidentId") int incidentId, HttpServletResponse response)
            throws ServletException, IOException{
        
        ModelAndView modelview = new ModelAndView("viewIncident");
        Authentication authentication = authenticationFacade.getAuthentication();
        modelview.getModelMap().addAttribute("userLogged", authentication.getPrincipal());
        modelview.getModelMap().addAttribute("userAuthority", systemUsersService.getUsersByName(authentication.getName()).getUserTypes());
        modelview.getModelMap().addAttribute("incidencia", incidentService.getIncidentById(incidentId));
        modelview.getModelMap().addAttribute("comentaris", commentsService.getCommentsByIncidenceId(incidentService.getIncidentById(incidentId)));
        return modelview;
    }
    
    @RequestMapping("/add")
    public ModelAndView addIncident(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        ModelAndView modelview = new ModelAndView("addIncident");
        Authentication authentication = authenticationFacade.getAuthentication();
        modelview.getModelMap().addAttribute("userLogged", authentication.getPrincipal());        
        Incident formAddIncident = new Incident();        
        modelview.getModelMap().addAttribute("formAddIncident", formAddIncident);
        modelview.getModelMap().addAttribute("userAuthority", systemUsersService.getUsersByName(authentication.getName()).getUserTypes());
        modelview.getModelMap().addAttribute("incidentTypes", incidentTypesService.getAllIncidentTypes());
        modelview.getModelMap().addAttribute("technics", systemUsersService.getAllTechnics());
        modelview.getModelMap().addAttribute("status", incidentService.getAllStatus());
        modelview.getModelMap().addAttribute("currentUser", systemUsersService.getUsersByName(authentication.getName()).getUserName());
        return modelview;
    }
    
    @RequestMapping(value = "/addIncident", method = RequestMethod.POST)
    public String processAddIncidentForm(@ModelAttribute("formAddIncident") Incident formAddIncident, BindingResult result) {
        String ret = "redirect:/home/incidents";
        if(formAddIncident.getTitle().isEmpty()){
            ret = "redirect:/home/incidents/addIncident/dadesBuides";
        } else {
            incidentService.addIncident(formAddIncident);
            ret = "redirect:/home/incidents";
        }
        return  ret;
    }
    
    @RequestMapping(value = "/addIncident/dadesBuides", method = RequestMethod.GET)
    public String dadesBuidesAdd (@ModelAttribute("formAddIncident") Incident formAddIncident, BindingResult result, Model model) {
        model.addAttribute("incorrectregister", "DADES INCORRECTES: Ompli tots els camps correctament i premi guardar");
        model.addAttribute("alertType", "alert alert-danger");
        model.addAttribute("error", "true");
        return "addIncident";
    }
    
    @RequestMapping(value = "/delete")
    public ModelAndView removeIncident (@RequestParam ("incidentId") int incidentId, HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        ModelAndView modelview = new ModelAndView("deleteIncident");
        Authentication authentication = authenticationFacade.getAuthentication();
        modelview.getModelMap().addAttribute("userLogged", authentication.getPrincipal());
        modelview.getModelMap().addAttribute("incidencia", incidentService.getIncidentById(incidentId));
        modelview.getModelMap().addAttribute("incidenciaID", incidentId);
        return modelview;
    }
    
    @RequestMapping(value = "/deleteIncident", method = RequestMethod.GET)
    public String deleteIncident (@RequestParam ("incidentId") int incidentId, HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        incidentService.deleteIncident(incidentService.getIncidentById(incidentId));
        return "redirect:/home/incidents";
    }
    
    @RequestMapping("/modifyIncident")
    public ModelAndView getModifyIncidentById(@RequestParam("incidentId") int incidentId, HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        ModelAndView modelview = new ModelAndView("modifyIncident");
        Authentication authentication = authenticationFacade.getAuthentication();
        modelview.getModelMap().addAttribute("userLogged", authentication.getPrincipal());
        modelview.getModelMap().addAttribute("userAuthority", systemUsersService.getUsersByName(authentication.getName()).getUserTypes());
        Incident formModifyIncident = incidentService.getIncidentById(incidentId);
        modelview.getModelMap().addAttribute("incidentTypes", incidentTypesService.getAllIncidentTypes());
        modelview.getModelMap().addAttribute("formModifyIncident", formModifyIncident);
        modelview.getModelMap().addAttribute("technics", systemUsersService.getAllTechnics());
        modelview.getModelMap().addAttribute("status", incidentService.getAllStatus());
        modelview.getModelMap().addAttribute("numIncident", incidentId);
        return modelview;
    }

    @RequestMapping(value = "/modifyIncident/update", method = RequestMethod.POST)
    public String processModifyIncidentForm(@ModelAttribute("formModifyIncident") Incident formModifyIncident, BindingResult result) {        
        String ret = "redirect:/home/incidents";
        String commentStatus = "Incidencia passada a estat ";
        Authentication authentication = authenticationFacade.getAuthentication();
        Comments commentForm = new Comments();
        commentForm.setCreatedBy(systemUsersService.getUsersByName(authentication.getName()));
        commentForm.setDescription(commentStatus);
        commentForm.setIncidentId(formModifyIncident);
        if (formModifyIncident.getStatus().equals("Tancada")&&(!formModifyIncident.getStatus().equals(incidentService.getIncidentById(formModifyIncident.getIncidentId()).getStatus()))) {
            commentStatus+=formModifyIncident.getStatus();
            commentForm.setDescription(commentStatus);
            Timestamp timestamp = new Timestamp(System.currentTimeMillis());
            formModifyIncident.setEndDate(timestamp);
            incidentService.updateIncident(formModifyIncident);            
            commentsService.addComment(commentForm);
            ret = "redirect:/home/incidents";
        } else if (!formModifyIncident.getStatus().equals(incidentService.getIncidentById(formModifyIncident.getIncidentId()).getStatus())) {
            commentStatus+=formModifyIncident.getStatus();
            commentForm.setDescription(commentStatus);
            commentsService.addComment(commentForm);
            incidentService.updateIncident(formModifyIncident);
            ret = "redirect:/home/incidents";
        }
        return  ret;
    }       
   
    @RequestMapping("/search")
    public ModelAndView searchIncident(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        ModelAndView modelview = new ModelAndView("searchIncident");
        Authentication authentication = authenticationFacade.getAuthentication();
        modelview.getModelMap().addAttribute("userLogged", authentication.getPrincipal());
        Incident formSearchIncident = new Incident();        
        modelview.getModelMap().addAttribute("formSearchIncident", formSearchIncident);
        modelview.getModelMap().addAttribute("userAuthority", systemUsersService.getUsersByName(authentication.getName()).getUserTypes());
        modelview.getModelMap().addAttribute("incidentTypes", incidentTypesService.getAllIncidentTypes());
        modelview.getModelMap().addAttribute("createdBy", systemUsersService.getAllSystemUsers());
        modelview.getModelMap().addAttribute("technics", systemUsersService.getAllTechnics());
        modelview.getModelMap().addAttribute("status", incidentService.getAllStatus());
        modelview.getModelMap().addAttribute("currentUser", systemUsersService.getUsersByName(authentication.getName()).getUserName());
        return modelview;
    }
    
    @RequestMapping(value = "/searchIncidents", method = RequestMethod.POST)
    public ModelAndView processSearchIncidentForm(@ModelAttribute("formSearchIncident") Incident formSearchIncident, BindingResult result) {
        ModelAndView modelview;
        Authentication authentication = authenticationFacade.getAuthentication();
        if(formSearchIncident.getTitle().isEmpty()&&formSearchIncident.getType().getNameType().equals("null")&& formSearchIncident.getCreationDate()==null && formSearchIncident.getEndDate()==null && formSearchIncident.getStatus().equals("null")&& formSearchIncident.getTechnicAssigned().getUserName().equals("null")){        
            modelview = new ModelAndView("searchIncident");
            modelview.getModelMap().addAttribute("userLogged", authentication.getPrincipal());
            modelview.getModelMap().addAttribute("formSearchIncident", formSearchIncident);
            modelview.getModelMap().addAttribute("userAuthority", systemUsersService.getUsersByName(authentication.getName()).getUserTypes());
            modelview.getModelMap().addAttribute("incidentTypes", incidentTypesService.getAllIncidentTypes());
            modelview.getModelMap().addAttribute("createdBy", systemUsersService.getAllSystemUsers());
            modelview.getModelMap().addAttribute("technics", systemUsersService.getAllTechnics());
            modelview.getModelMap().addAttribute("status", incidentService.getAllStatus());
            modelview.getModelMap().addAttribute("currentUser", systemUsersService.getUsersByName(authentication.getName()).getUserName());
            modelview.getModelMap().addAttribute("incorrectregister", "DADES INCORRECTES: Ompli algun dels criteris de cerca correctament i premi buscar");
            modelview.getModelMap().addAttribute("alertType", "alert alert-danger");
            modelview.getModelMap().addAttribute("error", "true");  
        } else {  
            modelview = new ModelAndView("resultSearch");
            modelview.getModelMap().addAttribute("userLogged", authentication.getPrincipal());
            modelview.getModelMap().addAttribute("userAuthority", systemUsersService.getUsersByName(authentication.getName()).getUserTypes());
            String nameAuth = systemUsersService.getUsersByName(authentication.getName()).getUserTypes();                          
            if (nameAuth.equals("Usuari")) {
                modelview.getModelMap().addAttribute("incidentList", incidentService.getSearchIncidentsByUser(formSearchIncident, systemUsersService.getUsersByName(authentication.getName())));
            } else {
                modelview.getModelMap().addAttribute("incidentList", incidentService.getSearchIncidents(formSearchIncident));
            }
        }
        return  modelview;
    }    
    
    @RequestMapping(value = "/addComment", method = RequestMethod.GET)
    public ModelAndView addComment(@RequestParam("incidentId") int incidentId, @RequestParam("close") String close, @RequestParam("reobrir") String reobrir, HttpServletResponse response)
            throws ServletException, IOException{        
        ModelAndView modelview = new ModelAndView("addComment");
        Comments formAddComment = new Comments();        
        modelview.getModelMap().addAttribute("formAddComment", formAddComment);
        Authentication authentication = authenticationFacade.getAuthentication();
        modelview.getModelMap().addAttribute("userLogged", authentication.getPrincipal());
        modelview.getModelMap().addAttribute("userAuthority", systemUsersService.getUsersByName(authentication.getName()).getUserTypes());
        modelview.getModelMap().addAttribute("incident", incidentService.getIncidentById(incidentId));
        modelview.getModelMap().addAttribute("close", close);
        modelview.getModelMap().addAttribute("reobrir", reobrir);
        return modelview;
    }
    
    @RequestMapping(value = "/processComment", method = RequestMethod.POST)
    public ModelAndView processAddCommentForm(@ModelAttribute("formAddComment") Comments formAddComment, @RequestParam("close") String close, @RequestParam("reobrir") String reobrir, BindingResult result) {
        ModelAndView modelview;
        Authentication authentication = authenticationFacade.getAuthentication();
        if(formAddComment.getDescription().isEmpty()){        
            modelview = new ModelAndView("addComment");
            modelview.getModelMap().addAttribute("userLogged", authentication.getPrincipal());
            modelview.getModelMap().addAttribute("formAddComment", formAddComment);
            modelview.getModelMap().addAttribute("userAuthority", systemUsersService.getUsersByName(authentication.getName()).getUserTypes());
            modelview.getModelMap().addAttribute("incident", incidentService.getIncidentById(formAddComment.getIncidentId().getIncidentId()));
            modelview.getModelMap().addAttribute("currentUser", systemUsersService.getUsersByName(authentication.getName()).getUserName());
            modelview.getModelMap().addAttribute("close", close);
            modelview.getModelMap().addAttribute("reobrir", reobrir);
            modelview.getModelMap().addAttribute("incorrectregister", "DADES INCORRECTES: Ompli el camp Comentari.");
            modelview.getModelMap().addAttribute("alertType", "alert alert-danger");
            modelview.getModelMap().addAttribute("error", "true");            
        } else {  
            formAddComment.setCreatedBy(systemUsersService.getUsersByName(authentication.getName()));
            formAddComment.setIncidentId(incidentService.getIncidentById(formAddComment.getIncidentId().getIncidentId()));
            commentsService.addComment(formAddComment);
            if (close.equals("true")){
                incidentService.closedIncident(incidentService.getIncidentById(formAddComment.getIncidentId().getIncidentId()).getIncidentId());
            }else if (reobrir.equals("true")){
                incidentService.openIncident(incidentService.getIncidentById(formAddComment.getIncidentId().getIncidentId()).getIncidentId());                
            }
            modelview = new ModelAndView("viewIncident");
            modelview.getModelMap().addAttribute("userLogged", authentication.getPrincipal());
            modelview.getModelMap().addAttribute("userAuthority", systemUsersService.getUsersByName(authentication.getName()).getUserTypes());
            modelview.getModelMap().addAttribute("incidencia", incidentService.getIncidentById(formAddComment.getIncidentId().getIncidentId()));
            modelview.getModelMap().addAttribute("comentaris", commentsService.getCommentsByIncidenceId(incidentService.getIncidentById(formAddComment.getIncidentId().getIncidentId())));
        }
        return  modelview;
    }
    
    @RequestMapping(value = "/deleteComment", method = RequestMethod.GET)
    public String deleteComment (@RequestParam ("commentId") int commentId, @RequestParam ("viewIncident") String incident, HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        commentsService.deleteComment(commentsService.getCommentById(commentId));        
        return incident;
    }
}