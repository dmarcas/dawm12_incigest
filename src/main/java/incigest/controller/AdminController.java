/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package incigest.controller;

import incigest.auth.IAuthenticationFacade;
import incigest.domain.IncidentTypes;
import incigest.domain.SystemUsers;
import incigest.service.IncidentService;
import incigest.service.IncidentTypesService;
import incigest.service.SystemUsersService;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;


@Controller
@RequestMapping(value = "/home/adminHome")
public class AdminController {
    
    @Autowired
    SystemUsersService systemUsersService;
    
    @Autowired
    IncidentService incidentService;
    
    @Autowired
    private IAuthenticationFacade authenticationFacade;
    
    @Autowired
    IncidentTypesService incidentTypesService;
    
    @RequestMapping(value = "", method = RequestMethod.GET)
    public ModelAndView adminPanelRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
                
        ModelAndView modelview = new ModelAndView("adminHome");
        Authentication authentication = authenticationFacade.getAuthentication();
        modelview.getModelMap().addAttribute("userLogged", authentication.getPrincipal());
        modelview.getModelMap().addAttribute("numUsuarisPendents", systemUsersService.getSystemUsersPendingConfirmation("Pendent").size());
        modelview.getModelMap().addAttribute("incidNoves",incidentService.getIncidentByStatus("Nova").size());
        modelview.getModelMap().addAttribute("incidEnCurs",incidentService.getIncidentByStatus("Resolucio").size());
        modelview.getModelMap().addAttribute("incidResoltes",incidentService.getIncidentByStatus("Solucionada").size());
        modelview.getModelMap().addAttribute("incidTancades",incidentService.getIncidentByStatus("Tancada").size());
        modelview.getModelMap().addAttribute("incidTotals", incidentService.getAllIncidents().size());
        return modelview;
    }
    
    @RequestMapping(value = "/users", method = RequestMethod.GET)
    public ModelAndView adminUsersView(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        ModelAndView modelview = new ModelAndView("users");
        Authentication authentication = authenticationFacade.getAuthentication();
        modelview.getModelMap().addAttribute("userLogged", authentication.getPrincipal());
        modelview.getModelMap().addAttribute("users", systemUsersService.getAllSystemUsers());
        modelview.getModelMap().addAttribute("usersPending", systemUsersService.getSystemUsersPendingConfirmation("Pendent"));
        return modelview;
    }
    
    @RequestMapping("/users/modifyusers")
    public ModelAndView getSystemUserByName(@RequestParam("userName") String userName, HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        ModelAndView modelview = new ModelAndView("modifyUsers");
        Authentication authentication = authenticationFacade.getAuthentication();
        modelview.getModelMap().addAttribute("userLogged", authentication.getPrincipal());
        modelview.getModelMap().addAttribute("userToModify", userName);
        SystemUsers formSystemUsers = systemUsersService.getUsersByName(userName);
        modelview.getModelMap().addAttribute("formModifyUsers", formSystemUsers);
        return modelview;
    }


    @RequestMapping(value = "/users/modifyusers/update", method = RequestMethod.POST)
    public String processUsersForm(@ModelAttribute("formModifyUsers") SystemUsers formModifyUsers, BindingResult result) {
        systemUsersService.updateSystemUsers(formModifyUsers);
        return "redirect:/home/adminHome/users";
    }
    
    @RequestMapping(value = "/incidentTypes", method = RequestMethod.GET)
    public ModelAndView typesIncidentsView(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        ModelAndView modelview = new ModelAndView("incidentTypes");
        Authentication authentication = authenticationFacade.getAuthentication();
        modelview.getModelMap().addAttribute("userLogged", authentication.getPrincipal());
        modelview.getModelMap().addAttribute("incidentTypes", incidentTypesService.getAllIncidentTypes());
        return modelview;
    }
    
    @RequestMapping("/incidentTypes/add")
    public ModelAndView addTypesIncident(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        ModelAndView modelview = new ModelAndView("addIncidentTypes");
        Authentication authentication = authenticationFacade.getAuthentication();
        modelview.getModelMap().addAttribute("userLogged", authentication.getPrincipal());
        IncidentTypes formAddIncidentTypes = new IncidentTypes();        
        modelview.getModelMap().addAttribute("formAddIncidentTypes", formAddIncidentTypes);        
        return modelview;
    }
    
    @RequestMapping(value = "/incidentTypes/addIncidentTypes", method = RequestMethod.POST)
    public String processAddTypesIncidentForm(@ModelAttribute("formAddIncidentTypes") IncidentTypes formAddIncidentTypes, BindingResult result) {
        String ret = "redirect:/";
        IncidentTypes nameIncidentType = incidentTypesService.getIncidentTypesByName(formAddIncidentTypes.getNameType());
        if(formAddIncidentTypes.getNameType().isEmpty()){
            ret = "redirect:/home/adminHome/incidentTypes/dadesBuides";
        } else if ((nameIncidentType != null) && (nameIncidentType.getNameType() == (formAddIncidentTypes.getNameType()))){
            ret = "redirect:/home/adminHome/incidentTypes/invalidIncidentTypes";
        } else {
            incidentTypesService.addIncidentTypes(formAddIncidentTypes);
            ret = "redirect:/home/adminHome/incidentTypes";
        }
        return  ret;
    }
    
    @RequestMapping(value = "/incidentTypes/dadesBuides", method = RequestMethod.GET)
    public String dadesBuidesAdd (@ModelAttribute("formAddIncidentTypes") IncidentTypes formAddIncidentTypes, BindingResult result, Model model) {
        model.addAttribute("incorrectregister", "DADES INCORRECTES: Ompli tots els camps correctament i premi guardar");
        model.addAttribute("alertType", "alert alert-danger");
        model.addAttribute("error", "true");
        return "addIncidentTypes";
    }
    
    @RequestMapping(value = "/incidentTypes/invalidIncidentTypes", method = RequestMethod.GET)
    public String invalidIncidentTypesAdd (@ModelAttribute("formAddIncidentTypes") IncidentTypes formAddIncidentTypes, BindingResult result, Model model) {
        model.addAttribute("incorrectregister", "DADES INCORRECTES: Ja existeix un tipus igual.");
        model.addAttribute("alertType", "alert alert-danger");
        model.addAttribute("error", "true");
        return "addIncidentTypes";
    }

    
    @RequestMapping(value = "/incidentTypes/modifyIncidentTypes", method = RequestMethod.GET)
    public ModelAndView getIncidentTypeByName(@RequestParam("nameType") String nameType, HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        ModelAndView modelview = new ModelAndView("modifyIncidentTypes");
        Authentication authentication = authenticationFacade.getAuthentication();
        modelview.getModelMap().addAttribute("userLogged", authentication.getPrincipal());
        IncidentTypes formIncidentTypes = incidentTypesService.getIncidentTypesByName(nameType);
        modelview.getModelMap().addAttribute("formModifyIncidentTypes", formIncidentTypes);
        return modelview;
    }
    
    @RequestMapping(value = "/incidentTypes/modifyIncidentTypes/update", method = RequestMethod.POST)
    public String processIncidentTypesForm(@ModelAttribute("formModifyIncidentTypes") IncidentTypes formModifyIncidentTypes, BindingResult result) {
        String ret = "redirect:/";
        if(formModifyIncidentTypes.getNameType().isEmpty()){
            ret = "redirect:/home/adminHome/incidentTypes/modifyIncidentTypes/dadesBuides";
        } else {
            incidentTypesService.updateIncidentTypes(formModifyIncidentTypes);
            return "redirect:/home/adminHome/incidentTypes";
        }
        return  ret;
    }
    
    @RequestMapping(value = "/incidentTypes/modifyIncidentTypes/dadesBuides", method = RequestMethod.GET)
    public String dadesBuidesModify (@ModelAttribute("formModifyIncidentTypes") IncidentTypes formModifyIncidentTypes, BindingResult result, Model model) {
        model.addAttribute("incorrectregister", "DADES INCORRECTES: Ompli tots els camps correctament i premi guardar");
        model.addAttribute("alertType", "alert alert-danger");
        model.addAttribute("error", "true");
        return "modifyIncidentTypes";
    }

    @RequestMapping(value = "incidentTypes/delete")
    public ModelAndView removeIncidentTypes (@RequestParam("nameType") String nameType, HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        ModelAndView modelview = new ModelAndView("deleteIncidentTypes");
        Authentication authentication = authenticationFacade.getAuthentication();
        modelview.getModelMap().addAttribute("userLogged", authentication.getPrincipal());
        IncidentTypes formIncidentTypes = incidentTypesService.getIncidentTypesByName(nameType);
        modelview.getModelMap().addAttribute("formDeleteIncidentTypes", formIncidentTypes);
        return modelview;
    }
    
    @RequestMapping(value = "incidentTypes/deleteIncidentTypes", method = RequestMethod.POST)
    public String deleteIncidentTypes (@ModelAttribute ("formDeleteIncidentTypes") IncidentTypes formModifyIncidentTypes, Model model){
        incidentTypesService.removeIncidentTypes(formModifyIncidentTypes);
        return "redirect:/home/adminHome/incidentTypes";
    }
    
    @RequestMapping(value = "/incidents/totalNoves")
    public ModelAndView totalNoves (HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        ModelAndView modelview = new ModelAndView("resultSearch");
        Authentication authentication = authenticationFacade.getAuthentication();
        modelview.getModelMap().addAttribute("userLogged", authentication.getPrincipal());
        modelview.getModelMap().addAttribute("userAuthority", systemUsersService.getUsersByName(authentication.getName()).getUserTypes());
        modelview.getModelMap().addAttribute("incidentList", incidentService.getIncidentByStatus("Nova"));
        return modelview;
    }
    
    @RequestMapping(value = "/incidents/totalEnCurs")
    public ModelAndView totalEnCurs (HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        ModelAndView modelview = new ModelAndView("resultSearch");
        Authentication authentication = authenticationFacade.getAuthentication();
        modelview.getModelMap().addAttribute("userLogged", authentication.getPrincipal());
        modelview.getModelMap().addAttribute("userAuthority", systemUsersService.getUsersByName(authentication.getName()).getUserTypes());
        modelview.getModelMap().addAttribute("incidentList", incidentService.getAllIncidentsEnCurs());
        return modelview;
    }
    
    @RequestMapping(value = "/incidents/totalResoltes")
    public ModelAndView totalResoltes (HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        ModelAndView modelview = new ModelAndView("resultSearch");
        Authentication authentication = authenticationFacade.getAuthentication();
        modelview.getModelMap().addAttribute("userLogged", authentication.getPrincipal());
        modelview.getModelMap().addAttribute("userAuthority", systemUsersService.getUsersByName(authentication.getName()).getUserTypes());
        modelview.getModelMap().addAttribute("incidentList", incidentService.getIncidentByStatus("Solucionada"));
        return modelview;
    }
    
    @RequestMapping(value = "/incidents/totalTancades")
    public ModelAndView totalTancades (HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        ModelAndView modelview = new ModelAndView("resultSearch");
        Authentication authentication = authenticationFacade.getAuthentication();
        modelview.getModelMap().addAttribute("userLogged", authentication.getPrincipal());
        modelview.getModelMap().addAttribute("userAuthority", systemUsersService.getUsersByName(authentication.getName()).getUserTypes());
        modelview.getModelMap().addAttribute("incidentList", incidentService.getIncidentByStatus("Tancada"));
        return modelview;
    }
    
    @RequestMapping(value = "/incidents/incidenciesTotals")
    public ModelAndView incidenciesTotals (HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        ModelAndView modelview = new ModelAndView("resultSearch");
        Authentication authentication = authenticationFacade.getAuthentication();
        modelview.getModelMap().addAttribute("userLogged", authentication.getPrincipal());
        modelview.getModelMap().addAttribute("userAuthority", systemUsersService.getUsersByName(authentication.getName()).getUserTypes());
        modelview.getModelMap().addAttribute("incidentList", incidentService.getAllIncidents());
        return modelview;
    }
}
