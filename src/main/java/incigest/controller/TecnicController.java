/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package incigest.controller;

import incigest.auth.IAuthenticationFacade;
import incigest.service.IncidentService;
import incigest.service.SystemUsersService;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;


@Controller
@RequestMapping(value = "/home/technicalHome")
public class TecnicController {
    
    @Autowired
    SystemUsersService systemUsersService;
    
    @Autowired
    IncidentService incidentService;
    
    @Autowired
    private IAuthenticationFacade authenticationFacade;
    
    @RequestMapping(value = "", method = RequestMethod.GET)
    public ModelAndView adminPanelRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        ModelAndView modelview = new ModelAndView("technicalHome");
        Authentication authentication = authenticationFacade.getAuthentication();
        modelview.getModelMap().addAttribute("userLogged", authentication.getPrincipal());
        modelview.getModelMap().addAttribute("incidNoves",incidentService.getIncidentNewByTechnic(systemUsersService.getUsersByName(authentication.getName())).size());
        modelview.getModelMap().addAttribute("incidEnCurs",incidentService.getIncidentsEnCursByTechnic(systemUsersService.getUsersByName(authentication.getName())).size());
        modelview.getModelMap().addAttribute("incidResoltes",incidentService.getIncidentsSolucionadaByTechnic(systemUsersService.getUsersByName(authentication.getName())).size());
        modelview.getModelMap().addAttribute("incidTancades",incidentService.getIncidentsTancatsByTechnic(systemUsersService.getUsersByName(authentication.getName())).size());
        modelview.getModelMap().addAttribute("incidTotals", incidentService.getIncidentByTechnic(systemUsersService.getUsersByName(authentication.getName())).size());
        return modelview;
    }
    
    @RequestMapping(value = "/incidents/totalNoves")
    public ModelAndView totalNoves (HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        ModelAndView modelview = new ModelAndView("resultSearch");
        Authentication authentication = authenticationFacade.getAuthentication();
        modelview.getModelMap().addAttribute("userLogged", authentication.getPrincipal());
        modelview.getModelMap().addAttribute("userAuthority", systemUsersService.getUsersByName(authentication.getName()).getUserTypes());
        modelview.getModelMap().addAttribute("incidentList", incidentService.getIncidentNewByTechnic(systemUsersService.getUsersByName(authentication.getName())));
        return modelview;
    }
    
    @RequestMapping(value = "/incidents/totalEnCurs")
    public ModelAndView totalEnCurs (HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        ModelAndView modelview = new ModelAndView("resultSearch");
        Authentication authentication = authenticationFacade.getAuthentication();
        modelview.getModelMap().addAttribute("userLogged", authentication.getPrincipal());
        modelview.getModelMap().addAttribute("userAuthority", systemUsersService.getUsersByName(authentication.getName()).getUserTypes());
        modelview.getModelMap().addAttribute("incidentList", incidentService.getIncidentsEnCursByTechnic(systemUsersService.getUsersByName(authentication.getName())));
        return modelview;
    }
    
    @RequestMapping(value = "/incidents/totalResoltes")
    public ModelAndView totalResoltes (HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        ModelAndView modelview = new ModelAndView("resultSearch");
        Authentication authentication = authenticationFacade.getAuthentication();
        modelview.getModelMap().addAttribute("userLogged", authentication.getPrincipal());
        modelview.getModelMap().addAttribute("userAuthority", systemUsersService.getUsersByName(authentication.getName()).getUserTypes());
        modelview.getModelMap().addAttribute("incidentList", incidentService.getIncidentsSolucionadaByTechnic(systemUsersService.getUsersByName(authentication.getName())));
        return modelview;
    }
    
    @RequestMapping(value = "/incidents/totalTancades")
    public ModelAndView totalTancades (HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        ModelAndView modelview = new ModelAndView("resultSearch");
        Authentication authentication = authenticationFacade.getAuthentication();
        modelview.getModelMap().addAttribute("userLogged", authentication.getPrincipal());
        modelview.getModelMap().addAttribute("userAuthority", systemUsersService.getUsersByName(authentication.getName()).getUserTypes());
        modelview.getModelMap().addAttribute("incidentList", incidentService.getIncidentsTancatsByTechnic(systemUsersService.getUsersByName(authentication.getName())));
        return modelview;
    }
    
    
    @RequestMapping(value = "/incidents/incidenciesTotals")
    public ModelAndView incidenciesTotals (HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        ModelAndView modelview = new ModelAndView("resultSearch");
        Authentication authentication = authenticationFacade.getAuthentication();
        modelview.getModelMap().addAttribute("userLogged", authentication.getPrincipal());
        modelview.getModelMap().addAttribute("userAuthority", systemUsersService.getUsersByName(authentication.getName()).getUserTypes());
        modelview.getModelMap().addAttribute("incidentList", incidentService.getIncidentByTechnic(systemUsersService.getUsersByName(authentication.getName())));
        return modelview;
    }
}
